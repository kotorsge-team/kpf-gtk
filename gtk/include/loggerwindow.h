#ifndef LOGGERWINDOW_H
#define LOGGERWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/textview.h>
#include <gtkmm/textbuffer.h>

#include <chrono>
#include <ctime>
#include <string>

class LoggerWindow : public Gtk::Window
{
public:
	LoggerWindow();
	virtual ~LoggerWindow();
	void append(Glib::ustring text);
	void timestamp(Glib::ustring text);
	Glib::ustring getLogContent();
	const std::string getDateTime();

private:
	Glib::RefPtr<Gtk::Builder> builder;

	Gtk::Box* boxContainer;
	Gtk::Button* bClose;
	Gtk::TextView* tvLog;

	Glib::RefPtr<Gtk::TextBuffer> buffer;

	Glib::ustring log_content;

	// signals
	void on_close_button_clicked();
};

#endif