#include <gtkmm/application.h>

#include "include/mainwindow.h"
#include "include/refs.h"

#include <iostream>
#include <string>
#include <sstream>
#include <map>

int runApp(int argc, char* argv[]);
int runAppNoGui(std::map<std::string, std::string>);

int main(int argc, char* argv[])
{
	std::map<std::string, std::string> argList;
	if(argc > 0)
	{
		for(int i = 0; i < argc; i++)
		{
			std::string arg(argv[i]);
			std::string prefix("--");
			if(arg.substr(0, prefix.size()) == prefix)
			{
				std::string val(arg.substr(prefix.size()));
				if(val == "nogui"
					|| val == "help")
				{
					argList[val] = std::string("true");
				}
				else
				{
					argList[val] = std::string(argv[i + 1]);
				}
			}
		}

		if(argList.count("nogui")
			|| argList.count("help"))
		{
			return runAppNoGui(argList);
		}
		else
		{
			return runApp(argc, argv);
		}
	}
	else
	{
		return runApp(argc, argv);
	}

	return 0;
}

int runApp(int argc, char* argv[])
{
	auto app = Gtk::Application::create(argc, argv,
		"com.kalebklein.kpf-gtk");

	MainWindow m;
	app->run(m);

	return 0;
}

int runAppNoGui(std::map<std::string, std::string> argList)
{
	for(auto i : argList)
	{
		std::string key(i.first.c_str());
		std::string val(i.second.c_str());

		if(key == "help")
		{
			std::stringstream help;
			std::string version = std::to_string(VERSION_MAJOR)
				+ "." + std::to_string(VERSION_MINOR);
			help << "KPF Linux v" << version << "\n"
				<< "KPF Linux is built in GTK+ and C++\n\n"
				<< "Example usage: kpf [OPTIONS] ...\n\n"
				<< "    --help - Shows this help text\n"
				<< "    --nogui - Runs KPF with no GUI and auto exports with defaults\n"
				<< "    --steam <PATH> - Uses given steam path with nogui option\n"
				<< "    --k2path <PATH> - Uses given KotOR 2 path with nogui option\n"
				<< "    --k2save <PATH> - Uses given KotOR 2 save path with nogui option\n"
				<< "    --k2cloud <PATH> - Uses given KotOR 2 cloud save path with nogui option\n"
				<< "    --noexport - Doesn't export paths but will display the INI results\n\n"
				<< "Examples:\n\n"
				<< "    kpf --nogui --steam ~/.steam\n"
				<< "    kpf --nogui --k2path ~/.steam/steamapps/common/Knights of the Old Republic II\n"
				<< "    kpf --nogui --noexport\n";

			std::cout << help.str().c_str() << std::endl;
		}
	}
}