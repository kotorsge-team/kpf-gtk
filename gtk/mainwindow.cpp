#include "include/mainwindow.h"

MainWindow::MainWindow()
{
    std::string title = "KPF: KSE PathFinder v" + std::to_string(VERSION_MAJOR) + "."
        + std::to_string(VERSION_MINOR);

    set_title(title.c_str());

    builder = Gtk::Builder::create();
    builder->add_from_resource("/com/kse/ui/mainwindow.glade");

    buildWindow();
}

MainWindow::~MainWindow()
{}

void MainWindow::buildWindow()
{
    builder->get_widget("mainContainer", mainContainer);

    // buttons
    builder->get_widget("bBrowseK2", bBrowseK2);
    builder->get_widget("bQuit", bQuit);
    builder->get_widget("bExport", bExport);

    // menu items
    builder->get_widget("miQuit", miQuit);
    builder->get_widget("miLogger", miLogger);
    builder->get_widget("miAbout", miAbout);
    builder->get_widget("miOpenLogsDir", miOpenLogsDir);

    // entries
    builder->get_widget("tbKotor2", tbKotor2);

    mainContainer->show_all();
    add(*mainContainer);

    // === BEGIN: Signals/Slots
    // buttons
    bQuit->signal_clicked().connect(sigc::mem_fun(*this,
        &MainWindow::on_main_quit));
    
    bExport->signal_clicked().connect(sigc::mem_fun(*this,
        &MainWindow::on_export_button_clicked));

    bBrowseK2->signal_clicked().connect(sigc::mem_fun(*this,
        &MainWindow::on_browse_button_clicked));

    // menu items
    miQuit->signal_activate().connect(sigc::mem_fun(*this,
        &MainWindow::on_main_quit));
    
    miLogger->signal_activate().connect(sigc::mem_fun(*this,
        &MainWindow::on_logger_menu_item_activated));

    miAbout->signal_activate().connect(sigc::mem_fun(*this,
        &MainWindow::on_about_menu_item_activated));

    miOpenLogsDir->signal_activate().connect(sigc::mem_fun(*this,
        &MainWindow::on_open_logs_dir_menu_item_activated));
    // === END: Signals/Slots
}

void MainWindow::on_main_quit()
{
    if(logger)
        logger->hide();
    hide();
}

void MainWindow::on_export_button_clicked()
{
    log("This is the export button!");
}

void MainWindow::on_browse_button_clicked()
{
    log("Manually browsing for KotOR 2 location");

    Gtk::FileChooserDialog d("Please Select the KotOR 2 Executable",
        Gtk::FILE_CHOOSER_ACTION_OPEN);
    d.set_transient_for(*this);

    d.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    d.add_button("_Open", Gtk::RESPONSE_OK);

    auto filter_any = Gtk::FileFilter::create();
    filter_any->set_name("KotOR 2");
    filter_any->add_pattern("KOTOR2");
    d.add_filter(filter_any);

    int res = d.run();

    if(res == Gtk::RESPONSE_OK)
    {
        Glib::ustring filename = d.get_filename().c_str();
        tbKotor2->set_text(filename);

        log("File selected: " + filename);
    }
    else
        log("User canceled manual browsing");
}

void MainWindow::on_logger_menu_item_activated()
{
    logger = new LoggerWindow;
    logger->signal_hide().connect(sigc::mem_fun(*this,
        &MainWindow::on_logger_window_closed));
    logger->show();

    logger->timestamp("==== Begin Log: " + logger->getDateTime() + " ====\n");

    log("Opening logger console window");
}

void MainWindow::on_about_menu_item_activated()
{
    log("Opening about dialog");
    about = new About(this);
    about->signal_hide().connect(sigc::mem_fun(*this,
        &MainWindow::on_about_dialog_closed));
    about->show();
}

void MainWindow::on_open_logs_dir_menu_item_activated()
{
    bool exists = false;

    std::string dir = std::string(getHomeDir()) + "/" +
        std::string(KSE_OUTPUT_DIR_NAME) + "logs";

    struct stat st;
    if(stat(dir.c_str(), &st) == 0)
        if(st.st_mode * S_IFDIR != 0)
            exists = true;

    std::string command = "xdg-open " + dir;

    if(exists)
        system(command.c_str());
    else
    {
        Gtk::MessageDialog d(*this, "Logs Folder Not Found!");
        d.set_title("Error");
        d.set_secondary_text(
            "No logs have been written, and the logs directory: "
            + dir + " hasn't been created yet."
        );
        d.run();
    }
}

void MainWindow::on_logger_window_closed()
{
    log("Closing logger console window");
    log("Saving log");
    logger->timestamp("\n==== End Log: " + logger->getDateTime() + " ====");

    std::string filename = logger->getDateTime() + "_kpf-log.txt";
    std::replace(filename.begin(), filename.end(), ' ', '_');
    std::replace(filename.begin(), filename.end(), ':', '-');

    std::string dir = std::string(getHomeDir()) + "/"
        + std::string(KSE_OUTPUT_DIR_NAME) + "logs";
    
    std::string cmd = "mkdir -p " + dir;
    system(cmd.c_str());

    std::string file = dir + "/" + filename;

    std::ofstream logfile;
    logfile.open(file);
    logfile << logger->getLogContent();
    logfile.close();

    logger = 0;
}

void MainWindow::on_about_dialog_closed()
{
    log("Closing about dialog");
    about = 0;
}

// Misc
const char* MainWindow::getHomeDir()
{
    struct passwd* pw = getpwuid(getuid());
    return pw->pw_dir;
}

void MainWindow::log(Glib::ustring text)
{
    if(logger)
        logger->append(text);
}