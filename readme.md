## To Built kpf-gtk
### Requirements
* GTK+ >= 3 - The GUI toolkit for Linux
* GTKMM >= 3 - C++ wrapper for GTK+

### Building
As long as you have the correct libraries installed (the two above) compiling and running is easy.

# kpf-gtk
pf-gtk if a GTK+ application built for KSE on Linux. kpf-gtk will soon be just KPF once it's complete. This repo will house the KPF project for Linux until it is complete and has been successfully interop-ed with KPF C# for Windows.

## Building
### Requirements
* GTK+ >= 3 - The GUI toolkit got Linux
* GTKMM >= 3 - C++ wrapper for GTK+

### Building
Create a build directory. It's not required, but highly recommended (It's cleaner)
```
$ mkdir -pv build
$ cd build
```
Next, you need to generate the Makefiles, and build the application
```
$ ../configure
$ make
```

If you wish to test the install, run `sudo make install`  
If not, look under the new `build/gtk` folder, and the kpf binary will be there  
If you did install it, `sudo make uninstall` will remove it